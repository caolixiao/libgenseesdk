
Pod::Spec.new do |s|


  s.name         = "libGenseePlayerSDK"
  s.version      = "1.0.0"
  s.summary      = "展示互动 PlayerSDK  版本-定制版"

  s.description  = <<-DESC
                    为了方便接入展示互动的SDK 
                   DESC

  s.homepage     = "http://www.gensee.com"
  
  s.license      = "MIT"
 
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.platform     = :ios, "8.0"

  s.ios.deployment_target = "8.0"
  
  s.source       = { :git => "git@bitbucket.org:caolixiao/libgenseesdk.git", :tag => "#{s.version}" }

  s.resource  = "RtPlayerSDK/PlayerSDK.bundle"

  s.ios.preserve_paths       = 'RtPlayerSDK/PlayerSDK.framework'
  s.ios.source_files         = 'RtPlayerSDK/PlayerSDK.framework/Versions/A/Headers/**/*.h'
  s.ios.public_header_files  = 'RtPlayerSDK/PlayerSDK.framework/Versions/A/Headers/**/*.h'
  s.ios.vendored_frameworks  = 'RtPlayerSDK/PlayerSDK.framework'
  
  s.frameworks = "OpenAL", "GLKit", "CoreVideo", "CoreMedia", "VideoToolbox", "SystemConfiguration"
  s.libraries = "iconv", "sqlite3.0", "z", "c++"
  # s.library   = "iconv"

  s.requires_arc = true

end
